from django.db.models import Q
from django.shortcuts import render
from django.db import IntegrityError
from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth.models import User
from django.contrib.auth import login, logout, authenticate
from django.utils import timezone
from django.contrib.auth.decorators import login_required
import colleagues
from workers.models import Worker
from django.contrib.auth.models import User


def home(request):
    if request.user.is_authenticated:
        all_workers = Worker.objects.all()
        workers = Worker.objects.all().filter(email=request.user.email)
        return render(request, "home.html", {'all_workers': all_workers, 'workers': workers})
    else:
        return render(request, "home.html")


def result(request):
        query = request.GET.get('search')
        object_list = Worker.objects.all().filter(Q(name=query) | Q(surname=query) | Q(email=query) | Q(position=query))
        return render(request, "result.html", {'object_list': object_list})


def signupuser(request):
    if request.method == 'GET':
        return render(request, "signupuser.html", {'form': UserCreationForm()})
    else:
        if request.POST['password1'] == request.POST['password2']:
            try:
                user = User.objects.create_user(request.POST['username'], password=request.POST['password1'],
                                                email=request.POST['email'])
                user.save()
                login(request, user)
                return redirect('home')
            except IntegrityError:
                return render(request, "signupuser.html",
                              {'form': UserCreationForm(), 'error': "This name or email has already been taken. "
                                                                    "Please. choose"
                                                                    "a new one"})
        else:
            return render(request, "signupuser.html",
                          {'form': UserCreationForm(), 'error': 'Password did not match'})


def loginuser(request):
    if request.method == 'GET':
        return render(request, "loginuser.html", {'form': AuthenticationForm()})
    else:
        user = authenticate(request, username=request.POST['username'], password=request.POST['password'])
        if user is None:
            return render(request, 'loginuser.html',
                          {'form': AuthenticationForm(), 'error': 'Username or password didn`t match'})
        else:
            login(request, user)
            return redirect('home')


@login_required
def logoutuser(request):
    if request.method == 'POST':
        logout(request)
        return redirect('home')


def my_custom_page_not_found_view(request, exception):
    return render(request, 'error404.html')
