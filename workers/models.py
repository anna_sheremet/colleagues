from datetime import date
from django.db import models
from django.contrib.auth.models import User

User._meta.get_field('email')._unique = True


class Worker(models.Model):
    name = models.CharField(max_length=50)
    surname = models.CharField(max_length=100)
    position = models.CharField(max_length=100)
    email = models.EmailField(max_length=100, unique=True)
    birth_date = models.DateField(default=date(1990, 1, 1))
    start_working_date = models.DateField(default=date.today())
